<?php
/**
 * Bootstrap file to launch the plugin.
 *
 * @wordpress-plugin
 * Plugin Name: Custom Blocks
 * Plugin URI:  https://github.com/Henryhankdc
 * Description: Plugin to create custom Gutenberg blocks
 * Version:     1.0.0
 * Author:      Henry Mesias
 * Author URI:  https://www.henrymesias.com
 * License:     GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

namespace Custom_Blocks\Blocks;

//  Exit if accessed directly.
defined('ABSPATH') || exit;
/**
 * Gets this plugin's absolute directory path.
 *
 * @since  2.1.0
 * @ignore
 * @access private
 *
 * @return string
 */
function _get_plugin_directory() {
	return __DIR__;
}
/**
 * Gets this plugin's URL.
 *
 * @since  2.1.0
 * @ignore
 * @access private
 *
 * @return string
 */
function _get_plugin_url() {
	static $plugin_url;
	if ( empty( $plugin_url ) ) {
		$plugin_url = plugins_url( null, __FILE__ );
	}
	return $plugin_url;
}

// Enqueue JS and CSS
include __DIR__ . '/lib/register-scripts.php';




