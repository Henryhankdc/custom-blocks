# Custom Gutenberg Block Plugin

This plugin is for development of custom Gutenberg Block development. The idea is for more blocks to be added over time. These blocks do not have to have opinionated styles, but simple markup/styles that can be customized easily on each project.

## Folder and File Overview

`./build/`: compiled front-end and editor assets.

`./lib/`: Create new files to register scripts, styles, etc here.

`./src/`: This is where the blocks directy lives, as well as styles and js for all blocks to be included live.

`./src/editor.scss`: Imported editor style sheet for each block.

`./src/style.scss`: Import front end style sheet for each block.

`./src/index.js`: Import blocks.

`./blocks/`: Each custom block will have it's own directory within this folder. _Example: `./blocks/example-block/`._

## Project Work

To use clone this directly into your plugins directory. Remove the directory for git `.git/`.

Activate the plugin in the WP-Admin. That's it. All active blocks will display in the Gutenberg Editor.

#### Develop

To build blocks `cd` into `plugins/custom-blocks/` and run `yarn install` to get all the dependencies you will need.

To start Webpack run `yarn start`.

To package things up for production run `yarn build`.

## Helpful Development Links

[Block Editor Handbook](https://developer.wordpress.org/block-editor/)

[Tutorials](https://developer.wordpress.org/block-editor/tutorials/)
