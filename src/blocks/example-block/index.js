/**
 *  BLOCK: Static Block
 *  ---
 *  Add details for a book to a post or page.
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

registerBlockType('threespotblock/static', {
  title: __('Example Static Block'),
  description: __('An Example Static Block.'),
  keywords: [
    __('container'),
    __('wrapper'),
    __('section')
  ],
  supports: {
    align: ['wide', 'full'],
    anchor: true,
    html: false,
  },
  category: 'common',
  icon: 'editor-kitchensink',

  edit: props => {
    const { className, isSelected } = props;
    return (
      <div className={className}>
        <h2>{__('Static Call to Action', 'threespotBlock')}</h2>
        <p>{__('This is really important!', 'threespotBlock')}</p>
        {
          isSelected && (
            <p className="sorry warning">{__('✋ Sorry! You cannot edit this block ✋', 'threespotBlock')}</p>
          )
        }
      </div>
    );
  },
  save: props => {
    return (
      <div>
        <h2>{__('Call to Action', 'threespotBlock')}</h2>
        <p>{__('This is really important!', 'threespotBlock')}</p>
      </div>
    );
  },
});
